import requests
import os
import logging
import time
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
for file in os.listdir("."):
    if file.endswith(".pcap"):
        print "Reading the pcap file:" + file + " and extracting urls from it" + "\n"

apikey = os.environ['vtapi']
url = 'https://www.virustotal.com/vtapi/v2/url/report'
list_url = []
packets = rdpcap('malicious.pcap')
for packet in packets:
    if packet.haslayer(DNSRR):
        if isinstance(packet.an, DNSRR):
            list_url.append(packet.an.rrname)

for list_of_urls in list_url:
    params = {'apikey': apikey,
              'resource': list_of_urls}

    response = requests.get(url, params=params)
    detected = []
    if response.json()['response_code'] == 1:
        for item in response.json()['scans']:
            if response.json()['scans'][item]['detected'] == True:
                detected.append(item)
        print "The url {} detected by following vendors: ".format(list_of_urls) + \
            ', '.join(detected) + "\n"
        time.sleep(5)
    else:
        print "The url {} is safe ".format(list_of_urls) + "\n"
        time.sleep(16)
