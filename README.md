# maliciouspcap
Python program to check malicious url's in a given pcap file using virustotal api.
This tool need virustotal api key.
Install `scapy`

[![asciicast](https://asciinema.org/a/eWgyURNqgFab9aCG4N04aRiuB.svg)](https://asciinema.org/a/eWgyURNqgFab9aCG4N04aRiuB)